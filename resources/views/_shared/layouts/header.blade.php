<!DOCTYPE html>
<html lang="en-US">

<head>
  <title>Travelz HTML5 Multipurpose Travel Template</title>
  <meta name="author" content="Nile-Theme">
  <meta name="robots" content="index follow">
  <meta name="googlebot" content="index follow">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="keywords" content="Travel, HTML5, CSS3, Hotel , Multipurpose, Template, Create a Travel website fast">
  <meta name="description" content="HTML5 Multipurpose Template, Create a website fast">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800|Poppins:300i,400,300,700,400i,500|Ubuntu:300i,400,300,700,400i,500|Raleway:400,500,600,700" rel="stylesheet">
  <!-- CSS Files -->

  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/plugins/revslider/css/settings.css') }}">
  <!-- Owl Carousel Assets -->
  <link href="{{ asset('public/assets/css/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/owl.theme.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('public/assets/fonts/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/travlez-jquery-ui.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/responsive.css') }}">

  <script type="text/javascript" src="{{ asset('public/assets/js/jquery-3.2.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/js/loading.js') }}"></script>

  <!-- REVOLUTION JS FILES -->
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/jquery.themepunch.tools.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/jquery.themepunch.revolution.min.js') }}"></script>

  <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.actions.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.migration.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/assets/plugins/revslider/js/extensions/revolution.extension.video.min.js') }}"></script>


</head>

<body>

  <!-- Loading bar -->
  <section id="load-screen">
    <!-- right id is ( load-screen ) -->
    <div class="looding-in background-light-grey padding-tb-200px z-index-99">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-lg-6 text-center">
            <!-- Loading -->
            <div class="loading-page">
              <div class="counter">
                <div class="text-center"><a href="05-home.html#"><img src="{{ asset('public/assets/images/logo.png') }}" alt=""></a></div>
                <div class="margin-tb-30px padding-8px background-white">
                  <div class="animation padding-2px background-main-color"></div>
                </div>
                <div>loading <span class="num">0%</span></div>
              </div>
            </div>
            <!-- // Loading -->
          </div>
        </div>
        <!-- // row -->
      </div>
      <!-- // container -->
    </div>
  </section>
  <!-- // Loading bar -->


  <!-- Header  -->
  <header>
    <div class="background-main-color padding-tb-5px">
      <div class="container">
        <div class="row">
          <div class="col-lg-2  d-none d-lg-block">
            <!-- lang dropdown -->
            <div class="dropdown show">
              <a class="dropdown-toggle text-white text-uppercase" href="05-home.html#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="flag-icon flag-icon-us margin-right-8px"></span> English
              </a>

              <div class="dropdown-menu text-small text-uppercase" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="05-home.html#"><span class="flag-icon flag-icon-es margin-right-8px"></span> Spanish</a>
                <a class="dropdown-item" href="05-home.html#"><span class="flag-icon flag-icon-mr margin-right-8px"></span> Arabic</a>
                <a class="dropdown-item" href="05-home.html#"><span class="flag-icon flag-icon-fr margin-right-8px"></span> French</a>
                <a class="dropdown-item" href="05-home.html#"><span class="flag-icon flag-icon-de margin-right-8px"></span> German</a>
              </div>
            </div>
            <!-- // lang dropdown -->
          </div>
          <div class="col-lg-3 col-md-12">
            <ul class="list-inline text-center margin-0px text-white">
              <li class="list-inline-item"><a class="facebook" href="05-home.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li class="list-inline-item"><a class="youtube" href="05-home.html#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
              <li class="list-inline-item"><a class="linkedin" href="05-home.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              <li class="list-inline-item"><a class="google" href="05-home.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li class="list-inline-item"><a class="twitter" href="05-home.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li class="list-inline-item"><a class="rss" href="05-home.html#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
            </ul>
            <!-- // Social -->
          </div>
          <div class="col-lg-4  d-none d-lg-block">
            <div class="contact-info text-white">
              <span class="margin-right-10px">Call us : 0022290411185</span>
              <span>Email: info@rabiie.com</span>
            </div>
          </div>
          <div class="col-lg-3  d-none d-lg-block">
            <ul class="user-area list-inline float-right margin-0px text-white">
              <li class="list-inline-item  padding-right-10px"><a href="https://nilethemes.com/html/travelz/page-login-2.html"><i class="fa fa-lock padding-right-5px"></i>login</a></li>
              <li class="list-inline-item"><a href="https://nilethemes.com/html/travelz/page-login-1.html"><i class="fa fa-user-plus padding-right-5px"></i>register</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="header-output">
      <div class="container header-in">
        <div class="row">
          <div class="col-lg-3">
            <a id="logo" href="https://nilethemes.com/html/travelz/01-home.html" class="d-inline-block margin-tb-10px"><img src="{{ asset('public/assets/images/logo.png') }}" alt=""></a>
            <a class="mobile-toggle" href="05-home.html#"><i class="fa fa-navicon"></i></a>
          </div>
          <div class="col-lg-9 position-inherit">

            <ul id="menu-main" class="nav-menu float-right link-padding-tb-20px">
              <li class="active mega-menu mega-links mega-links-4columns"><a href="05-home.html#">Home</a>
                <div class="mega-menu-out sub-menu-out">

                  <ul class="sub-menu-columns">
                    <li>
                      <a href="05-home.html#">demos one</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/01-home.html">Home - Agency 1</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/02-home.html">Home - Agency 2</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/03-home.html">Home - Agency 3</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/04-home.html">Home - Agency 4</a></li>
                        <li><a href="05-home.html">Home - Agency 5</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/06-home.html">Home - Agency 6</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/07-home.html">Home - Agency 7</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">demos Two</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/08-home.html">Home - Agency 8</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/09-home.html">Home - Agency 9</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/10-home.html">Home - Ocean Tours</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/11-home.html">Home - Anwaar Hotel</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/12-home.html">Home - Anwaar Hotel 2</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/13-home.html">Home - Taxi VIP</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/14-home.html">Home - City Taxi</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">demos three</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/15-home.html">Home - Blogger</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/16-home.html">Home - Blogger 2</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/17-home.html">Home - Sea World</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/18-home.html">Home - Sailing Tours</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/19-home.html">Home - Magazine</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/20-home.html">Home - Magazine 2</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/21-home.html">Home - Right Header</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">demos four</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/22-home.html">Home - Knoon Hotel</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/23-home.html">Home - Africa Travel</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/24-home.html">Home - Dark Style</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/25-home.html">Home - Search Header</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/26-home-one-page.html">Home - One page 1</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/27-home-one-page.html">Home - One page 2</a></li>
                      </ul>
                    </li>
                  </ul>

                </div>
              </li>
              <li class="has-dropdown"><a class="dropdown" href="05-home.html#">Features  </a>
                <ul class="sub-menu">
                  <li class="has-dropdown"><a href="05-home.html#">Headers</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-1.html">Header 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-2.html">Header 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-3.html">Header 3</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-4.html">Header 4</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-5.html">Header 5</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-6.html">Header 6</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-7.html">Header 7</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-8.html">Header 8</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-9.html">Header 9</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-10.html">Header 10</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-11.html">Header 11</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-12.html">Header 12</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/header-layout-13.html">Header 13</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">Footers</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-1.html">Footer 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-2.html">Footer 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-3.html">Footer 3</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-4.html">Footer 4</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-5.html">Footer 5</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-6.html">Footer 6</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-7.html">Footer 7</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-8.html">Footer 8</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-9.html">Footer 9</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/footer-layout-10.html">Footer 10</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">Gallery</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/gallery-full-width.html">Gallery Full Width</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/gallery-2-column.html">Gallery 2 Column</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/gallery-3-column.html">Gallery 3 Column</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/gallery-4-column.html">Gallery 4 Column</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">Shop</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/shop-2-col-left-sidebar.html">2 Col Left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-3-col-left-sidebar.html">3 Col Left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-2-col-right-sidebar.html">2 Col Right sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-3-col-right-sidebar.html">3 Col Right sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-3-col.html">3 Columns</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-4-col.html">4 Columns</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-cart.html">Cart</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-single.html">Single 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-single-2.html">Single 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/shop-single-2.html">Single 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">page title</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-title-1.html">page title 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-2.html">page title 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-3.html">page title 3</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-4.html">page title 4</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-5.html">page title 5</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-6.html">page title 6</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-7.html">page title 7</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-8.html">page title 8</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-9.html">page title 9</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-title-10.html">page title 10</a></li>
                    </ul>
                  </li>

                  <li class="has-dropdown"><a href="05-home.html#">Dashboard</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-home.html">Dashboard home</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-my-items.html">my items</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-my-favorites.html">my favorites</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-my-profile.html">my profile</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-blank.html">blank</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-404-page.html">404 page</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-packages.html">packages</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-reviews.html">reviews</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">Dashboard V2</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-home.html">Dashboard home</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-my-items.html">my items</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-my-favorites.html">my favorites</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-my-profile.html">my profile</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-blank.html">blank</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-404-page.html">404 page</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-packages.html">packages</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/dashboard-v2-reviews.html">reviews</a></li>
                    </ul>
                  </li>
                </ul>
              </li>

              <li class="has-dropdown"><a href="05-home.html#">Blog</a>
                <ul class="sub-menu">
                  <li class="has-dropdown"><a href="05-home.html#">Grid Layout</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/blog-grid-4-colm.html">Grid 4 Columns</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-grid-left-sidebar.html">grid left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-grid-no-sidebar.html">grid no sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-grid-right-sidebar.html">grid right sidebar</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">list Layout</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/blog-list-left-sidebar.html">list left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-list-no-sidebar.html">list no sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-list-right-sidebar.html">list right sidebar</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">classic Layout</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/blog-classic-left-sidebar.html">classic left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-classic-no-sidebar.html">classic no sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-classic-right-sidebar.html">classic right sidebar</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">single Layout</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/blog-single-left-sidebar.html">single left sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-single-no-sidebar.html">single no sidebar</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-single-right-sidebar.html">single right sidebar</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">Post Formats</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/blog-audio-post.html">audio post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-blockquote-post.html">blockquote post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-gallery-post.html">gallery post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-html5-video-post.html">video post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-slider-post.html">slider post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-standard-post.html">standard post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-vimeo-video-post.html">vimeo video post</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/blog-youtube-video-post.html">youtube video post</a></li>
                    </ul>
                  </li>
                </ul>
              </li>

              <li class="has-dropdown"><a href="05-home.html#">Pages</a>
                <ul class="sub-menu">
                  <li class="has-dropdown"><a href="05-home.html#">about</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-about.html">about 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-about-2.html">about 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-careers.html">careers</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-how-we-are.html">how we are</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">coming soon</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-coming-soon-1.html">coming soon 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-coming-soon-2.html">coming soon 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-coming-soon-3.html">coming soon 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">contact</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-contact-1.html">contact 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-contact-2.html">contact 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-contact-3.html">contact 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">faqs</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-1.html">faqs 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-2.html">faqs 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-3.html">faqs 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">404 page</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-1.html">404 page 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-2.html">404 page 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-faqs-3.html">404 page 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">loading page</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-loading-1.html">loading page 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-loading-2.html">loading page 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-loading-3.html">loading page 3</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-loading-4.html">loading page 4</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">login page</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-login-1.html">login page 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-login-2.html">login page 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-login-3.html">login page 3</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">pricing tables</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-pricing-tables-1.html">pricing tables 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-pricing-tables-2.html">pricing tables 2</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">services</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-services-1.html">page services 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-services-2.html">page services 2</a></li>
                    </ul>
                  </li>
                  <li class="has-dropdown"><a href="05-home.html#">team page</a>
                    <ul class="sub-menu">
                      <li><a href="https://nilethemes.com/html/travelz/page-team-1.html">team page 1</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-team-2.html">team page 2</a></li>
                      <li><a href="https://nilethemes.com/html/travelz/page-team-single.html">team single</a></li>
                    </ul>
                  </li>
                  <li><a href="https://nilethemes.com/html/travelz/page-maintenance.html">maintenance page</a></li>
                </ul>
              </li>
              <li class="has-dropdown"><a href="05-home.html#">Hotels</a>
                <ul class="sub-menu">
                  <li><a href="https://nilethemes.com/html/travelz/hotel-list.html">Hotels List View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/hotel-grid-1.html">Hotels Grid View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/hotel-grid-2.html">Hotels Grid View 2</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/hotel-details.html">Hotels Details</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/hotel-booking.html">Hotels Booking</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/hotel-thank-you.html">Hotels Thanks page</a></li>
                </ul>
              </li>
              <li class="has-dropdown"><a href="05-home.html#">Flights</a>
                <ul class="sub-menu">
                  <li><a href="https://nilethemes.com/html/travelz/flight-list.html">Flights List View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/flight-grid-1.html">Flights Grid View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/flight-grid-2.html">Flights Grid View 2</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/flight-details.html">Flights Details</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/flight-booking.html">Flights Booking</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/flight-thank-you.html">Flights Thanks page</a></li>
                </ul>
              </li>
              <li class="has-dropdown"><a href="05-home.html#">Cars</a>
                <ul class="sub-menu">
                  <li><a href="https://nilethemes.com/html/travelz/car-list.html">Cars List View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/car-grid-1.html">Cars Grid View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/car-grid-2.html">Cars Grid View 2</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/car-details.html">Cars Details</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/car-booking.html">Cars Booking</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/car-thank-you.html">Cars Thanks page</a></li>
                </ul>
              </li>
              <li class="has-dropdown"><a href="05-home.html#">tour</a>
                <ul class="sub-menu">
                  <li><a href="https://nilethemes.com/html/travelz/tour-list.html">tour List View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/tour-grid-1.html">tour Grid View</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/tour-grid-2.html">tour Grid View 2</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/tour-details.html">tour Details</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/tour-booking.html">tour Booking</a></li>
                  <li><a href="https://nilethemes.com/html/travelz/tour-thank-you.html">tour Thanks page</a></li>
                </ul>
              </li>
              <li class="mega-menu mega-links mega-links-4columns"><a href="05-home.html#">Elements</a>
                <div class="mega-menu-out sub-menu-out">
                  <ul class="sub-menu-columns">
                    <li>
                      <a href="05-home.html#">Elements One</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/elements-accordions.html">accordions</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-action-box.html">action box</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-alert-messages.html">alert messages</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-banners.html">banners</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-blockquotes.html">blockquotes</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-blog-post.html">blog post</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-booking-box.html">booking box</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">Elements Two</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/elements-box-style.html">box style</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-buttons.html">buttons</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-clients.html">clients</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-columns-grids.html">columns grids</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-counters.html">counters</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-datatable.html">datatable</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-flag-icon.html">flag icon</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">Elements three</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/elements-fontawesome.html">fontawesome</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-icon-style.html">icon style</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-labels-badges.html">labels badges</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-lists.html">lists</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-maps.html">maps</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-pagination.html">pagination</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-social-icons.html">social icons</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="05-home.html#">Elements four</a>
                      <ul class="mega-menu-list">
                        <li><a href="https://nilethemes.com/html/travelz/elements-tabs.html">tabs</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-team.html">team</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-typography.html">typography</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-video.html">video</a></li>
                        <li><a href="https://nilethemes.com/html/travelz/elements-timeline.html">timeline</a></li>




                      </ul>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- // Header  -->


  <style type="text/css">
  #rev_slider_6_1_wrapper .tp-loader.spinner4 {
    background-color: #FFFFFF !important;
  }

</style>
<style type="text/css">
.hephaistos .tp-bullet {
  width: 12px;
  height: 12px;
  position: absolute;
  background: rgba(153, 153, 153, 0);
  border: 3px solid rgba(255, 255, 255, 0.9);
  border-radius: 50%;
  cursor: pointer;
  box-sizing: content-box;
  box-shadow: 0px 0px 2px 1px rgba(130, 130, 130, 0.3)
}

.hephaistos .tp-bullet:hover,
.hephaistos .tp-bullet.selected {
  background: rgba(255, 255, 255, 0);
  border-color: rgba(220, 36, 38, 1)
}

</style>

<div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="travel-3" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
  <div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
    <ul>
      <!-- SLIDE  -->
      <li data-index="rs-15" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="680" data-thumb="assets/100x50_7f767-ba_8.jpg" data-delay="9580" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
        <img src="{{ asset('public/assets/images/7f767-ba_8.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-15-layer-14" data-x="['left','left','left','left']" data-hoffset="['-1000','-1000','-1000','-1000']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="5000" data-height="555" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;background-color:rgba(0, 38, 56, 0.67);"> </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption   tp-resizeme" id="slide-15-layer-4" data-x="['center','center','center','center']" data-hoffset="['1','1','3','-2']" data-y="['top','top','top','top']" data-voffset="['138','138','138','67']" data-fontsize="['20','20','20','15']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":620,"speed":780,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;tP:601;z:100;","ease":"Power3.easeInOut"},{"delay":"+7410","speed":620,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff;font-family:Raleway;">Make Professional Website </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme" id="slide-15-layer-5" data-x="['center','center','center','center']" data-hoffset="['-1','-1','0','-1']" data-y="['middle','middle','middle','middle']" data-voffset="['-77','-77','-47','-79']" data-fontsize="['60','60','60','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":880,"speed":870,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+6710","speed":600,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 700; color: #ffffff;font-family:Raleway;text-transform:uppercase;">great travel website </div>

        <!-- LAYER NR. 4 -->
        <div class="tp-caption   tp-resizeme" id="slide-15-layer-6" data-x="['center','center','center','center']" data-hoffset="['4','4','1','-1']" data-y="['top','top','top','top']" data-voffset="['268','268','249','172']" data-width="['610','610','610','320']" data-height="['none','none','none','98']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1160,"speed":960,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"+6070","speed":660,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 610px; max-width: 610px; white-space: normal; font-size: 13px; line-height: 22px; font-weight: 400; color: #ffffff;font-family:Open Sans;">Clean Code, Social Media , Google Web Fonts , Font Awesome Icons , Modern Design Bootstrap Framework , Free Support Documentation
        </div>

        <!-- LAYER NR. 5 -->
        <div class="tp-caption rev-btn " id="slide-15-layer-10" data-x="['center','center','center','center']" data-hoffset="['4','4','4','4']" data-y="['top','top','top','top']" data-voffset="['351','351','325','276']" data-width="199" data-height="42" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1580,"speed":990,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+5230","speed":710,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 9; min-width: 199px; max-width: 199px; max-width: 42px; max-width: 42px; white-space: normal; font-size: 15px; line-height: 17px; font-weight: 500; color: rgba(255,255,255,1);font-family:Roboto;text-transform:uppercase;background-color:rgba(243, 190, 78, 0.75);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Be special now
        </div>
      </li>
      <!-- SLIDE  -->
      <li data-index="rs-18" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="680" data-thumb="assets/100x50_e3842-ba_9.jpg" data-delay="9580" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
        <img src="{{ asset('public/assets/images/e3842-ba_9.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 6 -->
        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-18-layer-14" data-x="['left','left','left','left']" data-hoffset="['-1000','-1000','-1000','-1000']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="5000" data-height="555" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;background-color:rgba(0, 38, 56, 0.67);"> </div>

        <!-- LAYER NR. 7 -->
        <div class="tp-caption   tp-resizeme" id="slide-18-layer-4" data-x="['left','left','center','center']" data-hoffset="['56','56','3','-2']" data-y="['top','top','top','top']" data-voffset="['138','138','138','67']" data-fontsize="['20','20','20','15']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":620,"speed":780,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;tP:601;z:100;","ease":"Power3.easeInOut"},{"delay":"+7410","speed":620,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff;font-family:Raleway;">Make Professional Website </div>

        <!-- LAYER NR. 8 -->
        <div class="tp-caption   tp-resizeme" id="slide-18-layer-5" data-x="['left','left','center','center']" data-hoffset="['29','29','0','-1']" data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-47','-79']" data-fontsize="['45','45','40','30']" data-lineheight="['35','35','30','22']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":880,"speed":870,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+6710","speed":600,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 45px; line-height: 35px; font-weight: 700; color: #ffffff;font-family:Raleway;text-transform:uppercase;">easy booking <br>system design </div>

        <!-- LAYER NR. 9 -->
        <div class="tp-caption   tp-resizeme" id="slide-18-layer-6" data-x="['left','left','center','center']" data-hoffset="['30','30','1','-1']" data-y="['top','top','top','top']" data-voffset="['268','268','249','172']" data-width="['610','610','610','320']" data-height="['none','none','none','98']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1160,"speed":960,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"+6070","speed":660,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 610px; max-width: 610px; white-space: normal; font-size: 13px; line-height: 22px; font-weight: 400; color: #ffffff;font-family:Open Sans;">Clean Code, Social Media , Google Web Fonts , Font Awesome Icons , Modern Design Bootstrap Framework , Free Support Documentation
        </div>

        <!-- LAYER NR. 10 -->
        <div class="tp-caption rev-btn " id="slide-18-layer-10" data-x="['left','left','center','center']" data-hoffset="['30','30','0','4']" data-y="['top','top','top','top']" data-voffset="['351','351','325','276']" data-width="199" data-height="42" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1580,"speed":990,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+5230","speed":710,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 9; min-width: 199px; max-width: 199px; max-width: 42px; max-width: 42px; white-space: normal; font-size: 15px; line-height: 17px; font-weight: 500; color: rgba(255,255,255,1);font-family:Roboto;text-transform:uppercase;background-color:rgb(243,190,78);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Be special now
        </div>
      </li>
      <!-- SLIDE  -->
      <li data-index="rs-19" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="680" data-thumb="assets/100x50_a8dd4-ba_2.jpg" data-delay="9580" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
        <img src="{{ asset('public/assets/images/a8dd4-ba_2.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 11 -->
        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-19-layer-14" data-x="['left','left','left','left']" data-hoffset="['-1000','-1000','-1000','-1000']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="5000" data-height="555" data-whitespace="nowrap" data-type="shape" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":380,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;background-color:rgba(255, 227, 214, 0.67);"> </div>

        <!-- LAYER NR. 12 -->
        <div class="tp-caption   tp-resizeme" id="slide-19-layer-4" data-x="['center','center','center','center']" data-hoffset="['1','1','3','-2']" data-y="['top','top','top','top']" data-voffset="['138','138','138','67']" data-fontsize="['20','20','20','15']" data-color="['rgb(61,61,61)','rgb(61,61,61)','rgb(255,255,255)','rgb(255,255,255)']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":620,"speed":780,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;tP:601;z:100;","ease":"Power3.easeInOut"},{"delay":"+7410","speed":620,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #3d3d3d;font-family:Raleway;">Make Professional Website </div>

        <!-- LAYER NR. 13 -->
        <div class="tp-caption   tp-resizeme" id="slide-19-layer-5" data-x="['center','center','center','center']" data-hoffset="['-1','-1','0','-1']" data-y="['middle','middle','middle','middle']" data-voffset="['-77','-77','-47','-79']" data-fontsize="['60','60','60','30']" data-color="['rgb(0,0,0)','rgb(0,0,0)','rgb(255,255,255)','rgb(255,255,255)']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":880,"speed":870,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+6710","speed":600,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 700; color: #000000;font-family:Raleway;text-transform:uppercase;">great travel website </div>

        <!-- LAYER NR. 14 -->
        <div class="tp-caption   tp-resizeme" id="slide-19-layer-6" data-x="['center','center','center','center']" data-hoffset="['4','4','1','-1']" data-y="['top','top','top','top']" data-voffset="['268','268','249','172']" data-color="['rgb(66,66,66)','rgb(66,66,66)','rgb(255,255,255)','rgb(255,255,255)']" data-width="['610','610','610','320']" data-height="['none','none','none','98']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1160,"speed":960,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"+6070","speed":660,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 610px; max-width: 610px; white-space: normal; font-size: 13px; line-height: 22px; font-weight: 400; color: #424242;font-family:Open Sans;">Clean Code, Social Media , Google Web Fonts , Font Awesome Icons , Modern Design Bootstrap Framework , Free Support Documentation
        </div>

        <!-- LAYER NR. 15 -->
        <div class="tp-caption rev-btn " id="slide-19-layer-10" data-x="['center','center','center','center']" data-hoffset="['4','4','4','4']" data-y="['top','top','top','top']" data-voffset="['351','351','325','276']" data-width="199" data-height="42" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1580,"speed":990,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+5230","speed":710,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 9; min-width: 199px; max-width: 199px; max-width: 42px; max-width: 42px; white-space: normal; font-size: 15px; line-height: 17px; font-weight: 500; color: rgba(255,255,255,1);font-family:Roboto;text-transform:uppercase;background-color:rgb(243,190,78);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Be special now
        </div>
      </li>
    </ul>
    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
  </div>
</div>
