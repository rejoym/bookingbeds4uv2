 <!-- Footer -->
 <footer class="text-center text-lg-left">
  <div class="container">
    <div class="row padding-tb-100px">
      <div class="col-lg-6 sm-mb-35px">
        <div class="about">
          <div class="logo margin-bottom-20px"><a href="05-home.html#"><img src="{{ asset('public/assets/images/logo.png') }}" alt=""></a></div>
          <p class="text-grey-2">
            Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida at, accumsan bibendum tincidunt imperdiet. Maecenas quis magna faucibus, varius ante sit amet, varius augue. Praesent aliquam, a imperdiet lacus libero ac tellus. Nunc fringilla ullamcorper quam at lacinia.
          </p>
        </div>
      </div>
      <div class="col-lg-3 sm-mb-35px">
        <ul class="footer-menu row margin-0px padding-0px list-unstyled">
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Home</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Featured</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Feedback</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Ask a Question</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Team</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Maintenance</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Get a Quote</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Contact Us</a></li>
          <li class="col-6  padding-tb-5px"><a href="05-home.html#" class="text-main-color">Alerts messages</a></li>
        </ul>
      </div>
      <div class="col-lg-3">
        <ul class="images-feed row no-gutters margin-0px padding-0px list-unstyled">
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-1.jpg') }}" alt=""></a></li>
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-2.jpg') }}" alt=""></a></li>
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-3.jpg') }}" alt=""></a></li>
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-4.jpg') }}" alt=""></a></li>
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-5.jpg') }}" alt=""></a></li>
          <li class="col-4 padding-tb-5px"><a href="05-home.html#" class="padding-lr-5px d-block"><img src="{{ asset('public/assets/demo-img/instagram-6.jpg') }}" alt=""></a></li>
        </ul>
      </div>
    </div>

    <div class="row padding-tb-30px border-top-1 border-grey-1">
      <div class="col-lg-4">
        <p class="text-lg-left"><span class="text-third-color">Travelz</span> | @2017 All copy rights reserved</p>
      </div>
      <div class="col-lg-4 sm-mb-20px">
        <div class="text-center"><img src="{{ asset('public/assets/images/cards.png') }}" alt=""></div>
      </div>
      <div class="col-lg-4">
        <ul class="social_link list-inline text-sm-center text-lg-right">
          <li class="list-inline-item"><a class="facebook" href="05-home.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li class="list-inline-item"><a class="youtube" href="05-home.html#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
          <li class="list-inline-item"><a class="linkedin" href="05-home.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          <li class="list-inline-item"><a class="google" href="05-home.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
          <li class="list-inline-item"><a class="twitter" href="05-home.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li class="list-inline-item"><a class="rss" href="05-home.html#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
        </ul>
        <!-- // Social -->
      </div>
    </div>

  </div>

</footer>
<!-- // Footer -->
<!-- END REVOLUTION SLIDER -->
<script type="text/javascript">
  var tpj = jQuery;

  var revapi6;
  tpj(document).ready(function() {
    if (tpj("#rev_slider_6_1").revolution == undefined) {
      revslider_showDoubleJqueryError("#rev_slider_6_1");
    } else {
      revapi6 = tpj("#rev_slider_6_1").show().revolution({
        sliderType: "standard",
        jsFileLocation: "//localhost/revslider-standalone/revslider/public/assets/js/",
        sliderLayout: "fullwidth",
        dottedOverlay: "none",
        delay: 9000,
        navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          mouseScrollReverse: "default",
          onHoverStop: "off",
          bullets: {
            enable: true,
            hide_onmobile: false,
            style: "hephaistos",
            hide_onleave: false,
            direction: "horizontal",
            h_align: "center",
            v_align: "top",
            h_offset: 0,
            v_offset: 30,
            space: 5,
            tmp: ''
          }
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1170, 900, 778, 480],
        gridheight: [555, 555, 500, 400],
        lazyType: "none",
        shadow: 0,
        spinner: "spinner4",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: "off",
          nextSlideOnWindowFocus: "off",
          disableFocusListener: false,
        }
      });
    }
  }); /*ready*/

</script>

<script type="text/javascript" src="{{ asset('public/assets/js/sticky-sidebar.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/custom.js') }}"></script>
<script src="{{ asset('public/assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>


</body>

</html>
