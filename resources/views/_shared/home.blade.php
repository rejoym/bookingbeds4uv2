@extends('_shared.layouts.master')
@section('main-content')
<!-- Search Filter  -->
<div class="background-light-grey">
  <div class="container">
    <div id="search-filter-2" class="search-filter pull-top-80px z-index-5 position-relative">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item margin-right-8px">
          <a class="nav-link active background-third-color text-white" data-toggle="tab" href="05-home.html#home" role="tab"><i class="fa fa-hotel"></i> Hotels</a>
        </li>
        <li class="nav-item margin-right-8px">
          <a class="nav-link background-third-color text-white" data-toggle="tab" href="05-home.html#profile" role="tab"><i class="fa fa-plane"></i> Flights</a>
        </li>
        <li class="nav-item margin-right-8px">
          <a class="nav-link background-third-color text-white" data-toggle="tab" href="05-home.html#messages" role="tab"><i class="fa fa-cab"></i> Cars</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content background-white padding-30px box-shadow">

        <div class="tab-pane active" id="home" role="tabpanel">
          <div class="row">
            <div class="form-group col-lg-2">
              <label>Destination</label>
              <div class="destination"><input type="text" class="input-text full-width" placeholder="Enter Destination"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="col-lg-2">
              <label>Rooms</label>
              <div class="rooms"><input type="text" class="input-text full-width" placeholder="1"></div>
            </div>
            <div class="col-lg-2">
              <label>Children</label>
              <div class="children"><input type="text" class="input-text full-width" placeholder="0"></div>
            </div>
            <div class="col-lg-2">
              <a href="05-home.html#" class="btn-sm btn-lg btn-block background-main-color text-white text-center text-uppercase font-weight-600 margin-top-30px padding-7px"> Hotel  Search</a>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="tab-pane" id="profile" role="tabpanel">
          <div class="row">
            <div class="form-group col-lg-2">
              <label>Destination</label>
              <div class="destination"><input type="text" class="input-text full-width" placeholder="Enter Destination"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="col-lg-2">
              <label>Rooms</label>
              <div class="rooms"><input type="text" class="input-text full-width" placeholder="1"></div>
            </div>
            <div class="col-lg-2">
              <label>Children</label>
              <div class="children"><input type="text" class="input-text full-width" placeholder="0"></div>
            </div>
            <div class="col-lg-2">
              <a href="05-home.html#" class="btn-sm btn-lg btn-block background-main-color text-white text-center text-uppercase font-weight-600 margin-top-30px padding-7px"> Hotel  Search</a>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="tab-pane" id="messages" role="tabpanel">
          <div class="row">
            <div class="form-group col-lg-2">
              <label>Destination</label>
              <div class="destination"><input type="text" class="input-text full-width" placeholder="Enter Destination"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="form-group col-lg-2">
              <label>Check Out</label>
              <div class="date-input"><input type="text" class="input-text datepicker full-width" placeholder="15 / 5 / 2017"></div>
            </div>
            <div class="col-lg-2">
              <label>Rooms</label>
              <div class="rooms"><input type="text" class="input-text full-width" placeholder="1"></div>
            </div>
            <div class="col-lg-2">
              <label>Children</label>
              <div class="children"><input type="text" class="input-text full-width" placeholder="0"></div>
            </div>
            <div class="col-lg-2">
              <a href="05-home.html#" class="btn-sm btn-lg btn-block background-main-color text-white text-center text-uppercase font-weight-600 margin-top-30px padding-7px"> Hotel  Search</a>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>
<!-- // Search Filter   -->

<section class="padding-tb-100px background-light-grey">
  <div class="container">
    <div class="services-out">
      <div class="row">

        <div class="col-lg-3 col-md-6 sm-mb-35px">
          <h3><i class="fa fa-building-o icon-round"></i>
            <span class="text-medium text-uppercase">Booking of hotels</span></h3>
            <i class="d-block text-up-small text-grey-2 margin-bottom-15px">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero congue arcu.</i>
            <a href="05-home.html#" class="btn background-main-color text-white padding-lr-10px text-small text-uppercase">Read more</a>
          </div>


          <div class="col-lg-3 col-md-6 sm-mb-35px">
            <h3><i class="fa fa-automobile icon-round"></i>
              <span class="text-medium text-uppercase">Car Rental</span></h3>
              <i class="d-block text-up-small text-grey-2 margin-bottom-15px">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero congue arcu.</i>
              <a href="05-home.html#" class="btn background-main-color text-white padding-lr-10px text-small text-uppercase">Read more</a>
            </div>


            <div class="col-lg-3 col-md-6 sm-mb-35px">
              <h3><i class="fa fa-plane icon-round"></i>
                <span class="text-medium text-uppercase">booking flight</span></h3>
                <i class="d-block text-up-small text-grey-2 margin-bottom-15px">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero congue arcu.</i>
                <a href="05-home.html#" class="btn background-main-color text-white padding-lr-10px text-small text-uppercase">Read more</a>
              </div>



              <div class="col-lg-3 col-md-6 sm-mb-35px">
                <h3><i class="fa fa-umbrella icon-round"></i>
                  <span class="text-medium text-uppercase">Tourist services</span></h3>
                  <i class="d-block text-up-small text-grey-2 margin-bottom-15px">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero congue arcu.</i>
                  <a href="05-home.html#" class="btn background-main-color text-white padding-lr-10px text-small text-uppercase">Read more</a>
                </div>

              </div>
            </div>
          </div>
        </section>

        <!-- Hot Hotels -->
        <section id="hot-hotels" class="padding-top-100px padding-bottom-70px background-grey-1">
          <div class="container">
            <div class="section-title section-title-center">
              <h1 class="title"><span>Hot</span> Hotels</h1>
              <span class="section-title-des">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elitdunt</span>
            </div>

            <div class="row">

              <!-- hotel item ( 1 ) -->
              <div class="col-lg-6 margin-bottom-30px">
                <div class="hotel-list background-white border border-grey-1">
                  <div class="hotel-thum">
                    <ul class="service">
                      <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Wifi"><i class="fa fa-wifi"></i></a></li>
                      <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Television"><i class="fa fa-television"></i></a></li>
                      <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Newspaper"><i class="fa fa-newspaper-o"></i></a></li>
                      <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Car Parking"><i class="fa fa-automobile"></i></a></li>
                    </ul>
                    <div class="img-in">
                      <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/hotel-list-1.jpg') }}" alt=""></a>
                    </div>
                  </div>
                  <div class="padding-30px">
                    <a href="05-home.html#" class="text-dark d-block text-uppercase text-medium font-weight-600 margin-bottom-5px">Adel New Hotel</a>
                    <small class="text-uppercase text-extra-small  d-block">
                      <a href="05-home.html#" class="text-grey-4"><i class="fa fa-map-marker margin-right-5px"></i>
                        <span class="text-third-color margin-right-5px">United Kingdom</span> London</a>
                      </small>
                      <p class="text-grey-2 margin-top-8px">Many desktop publishing packages and web page editors now use Lorem Ipsum </p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>


                <!-- hotel item ( 2 ) -->
                <div class="col-lg-6  margin-bottom-30px">
                  <div class="hotel-list background-white border border-grey-1">
                    <div class="hotel-thum">
                      <ul class="service">
                        <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Wifi"><i class="fa fa-wifi"></i></a></li>
                        <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Television"><i class="fa fa-television"></i></a></li>
                        <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Newspaper"><i class="fa fa-newspaper-o"></i></a></li>
                        <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Car Parking"><i class="fa fa-automobile"></i></a></li>
                      </ul>
                      <div class="img-in">
                        <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/hotel-list-2.jpg') }}" alt=""></a>
                      </div>
                    </div>
                    <div class="padding-30px">
                      <a href="05-home.html#" class="text-dark d-block text-uppercase text-medium font-weight-600 margin-bottom-5px">Adel New Hotel</a>
                      <small class="text-uppercase text-extra-small  d-block">
                        <a href="05-home.html#" class="text-grey-4"><i class="fa fa-map-marker margin-right-5px"></i>
                          <span class="text-third-color margin-right-5px">United Kingdom</span> London</a>
                        </small>
                        <p class="text-grey-2 margin-top-8px">Many desktop publishing packages and web page editors now use Lorem Ipsum </p>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>


                  <!-- hotel item ( 3 ) -->
                  <div class="col-lg-6  margin-bottom-30px">
                    <div class="hotel-list background-white border border-grey-1">
                      <div class="hotel-thum">
                        <ul class="service">
                          <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Wifi"><i class="fa fa-wifi"></i></a></li>
                          <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Television"><i class="fa fa-television"></i></a></li>
                          <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Newspaper"><i class="fa fa-newspaper-o"></i></a></li>
                          <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Car Parking"><i class="fa fa-automobile"></i></a></li>
                        </ul>
                        <div class="img-in">
                          <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/hotel-list-3.jpg') }}" alt=""></a>
                        </div>
                      </div>
                      <div class="padding-30px">
                        <a href="05-home.html#" class="text-dark d-block text-uppercase text-medium font-weight-600 margin-bottom-5px">Adel New Hotel</a>
                        <small class="text-uppercase text-extra-small  d-block">
                          <a href="05-home.html#" class="text-grey-4"><i class="fa fa-map-marker margin-right-5px"></i>
                            <span class="text-third-color margin-right-5px">United Kingdom</span> London</a>
                          </small>
                          <p class="text-grey-2 margin-top-8px">Many desktop publishing packages and web page editors now use Lorem Ipsum </p>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>


                    <!-- hotel item ( 4 ) -->
                    <div class="col-lg-6  margin-bottom-30px">
                      <div class="hotel-list background-white border border-grey-1">
                        <div class="hotel-thum">
                          <ul class="service">
                            <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Wifi"><i class="fa fa-wifi"></i></a></li>
                            <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Television"><i class="fa fa-television"></i></a></li>
                            <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Newspaper"><i class="fa fa-newspaper-o"></i></a></li>
                            <li class="service-on"><a href="05-home.html#" data-toggle="tooltip" data-placement="left" title="Car Parking"><i class="fa fa-automobile"></i></a></li>
                          </ul>
                          <div class="img-in">
                            <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/hotel-list-4.jpg') }}" alt=""></a>
                          </div>
                        </div>
                        <div class="padding-30px">
                          <a href="05-home.html#" class="text-dark d-block text-uppercase text-medium font-weight-600 margin-bottom-5px">Adel New Hotel</a>
                          <small class="text-uppercase text-extra-small  d-block">
                            <a href="05-home.html#" class="text-grey-4"><i class="fa fa-map-marker margin-right-5px"></i>
                              <span class="text-third-color margin-right-5px">United Kingdom</span> London</a>
                            </small>
                            <p class="text-grey-2 margin-top-8px">Many desktop publishing packages and web page editors now use Lorem Ipsum </p>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>


                    </div>

                  </div>
                </section>
                <!-- // Hot Hotels -->



                <section class="padding-tb-100px background-light-grey">
                  <div class="container">

                    <div class="section-title section-title-center">
                      <h1 class="title"><span>News &amp; </span> Events</h1>
                      <span class="section-title-des">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elitdunt</span>
                    </div>

                    <div class="row">
                      <div class="col-sm-4 sm-mb-35px">
                        <div class="background-white border border-grey">
                          <div class="post-img">
                            <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/blog-1.jpg') }}" alt=""></a>
                          </div>
                          <div class="padding-30px">
                            <a href="05-home.html#" class="d-block text-dark text-uppercase text-medium margin-bottom-15px font-weight-700">13 Non-Travel Books That Changed My Life</a>
                            <span class="margin-right-20px text-extra-small">By : <a href="05-home.html#" class="text-main-color">Rabie Elkheir</a></span>
                            <span class="text-extra-small">Date :  <a href="05-home.html#"  class="text-main-color">July 15, 2016</a></span>
                            <p class="text-grey-2 margin-top-8px">up a land of wild nature, mystical and unexplored. With only 350,000 visitors per year, Madagascar is one of the most well-known but least visited </p>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4 sm-mb-35px">
                        <div class="background-white border border-grey">
                          <div class="post-img">
                            <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/blog-2.jpg') }}" alt=""></a>
                          </div>
                          <div class="padding-30px">
                            <a href="05-home.html#" class="d-block text-dark text-uppercase text-medium margin-bottom-15px font-weight-700">30 Epic Photos From My Trip to Madagascar</a>
                            <span class="margin-right-20px text-extra-small">By : <a href="05-home.html#" class="text-main-color">Rabie Elkheir</a></span>
                            <span class="text-extra-small">Date :  <a href="05-home.html#"  class="text-main-color">July 15, 2016</a></span>
                            <p class="text-grey-2 margin-top-8px">up a land of wild nature, mystical and unexplored. With only 350,000 visitors per year, Madagascar is one of the most well-known but least visited </p>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4 sm-mb-35px">
                        <div class="background-white border border-grey">
                          <div class="post-img">
                            <a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/blog-3.jpg') }}" alt=""></a>
                          </div>
                          <div class="padding-30px">
                            <a href="05-home.html#" class="d-block text-dark text-uppercase text-medium margin-bottom-15px font-weight-700">Is Travel Hacking Really a Scam?</a>
                            <span class="margin-right-20px text-extra-small">By : <a href="05-home.html#" class="text-main-color">Rabie Elkheir</a></span>
                            <span class="text-extra-small">Date :  <a href="05-home.html#"  class="text-main-color">July 15, 2016</a></span>
                            <p class="text-grey-2 margin-top-8px">up a land of wild nature, mystical and unexplored. With only 350,000 visitors per year, Madagascar is one of the most well-known but least visited </p>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </section>

                <!-- Instgram Feed -->
                <div id="instgram-feed">
                  <ul class="instagram-feed">
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-1.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-2.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-3.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-4.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-5.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-6.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-7.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-8.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-9.jpg') }}" alt=""></a></li>
                    <li><a href="05-home.html#"><img src="{{ asset('public/assets/demo-img/instagram-10.jp') }}g" alt=""></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <!-- // Instgram Feed -->

                @stop