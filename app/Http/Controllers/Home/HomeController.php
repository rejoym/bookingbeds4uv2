<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * function to load the landing page
     * @return view [description]
     */
    public function getIndex()
    {
      return view('_shared.home');
    }
}
